#include <stdlib.h>
#include <stdio.h>
#include <error.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>

#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <net/if.h>
#include <linux/if_link.h>

#include "../ebpf/xdp_drop_ssh_stats.h"

static const char *xdp_filename = "../ebpf/xdp_drop_ssh_stats.o";
static const char *dev_name = "enp0s9";
static const int  xdp_flags  = XDP_FLAGS_SKB_MODE;

static volatile int isRunning = 1;

struct bpf_object *load_bpf_obj(const char *filename)
{
  struct bpf_object *obj = NULL;
  int prog_fd = -1;
  int err;
  int ifindex;

  struct bpf_prog_load_attr prog_load_attr = {
    .prog_type = BPF_PROG_TYPE_XDP,
    .file = filename,
  };

  err = bpf_prog_load_xattr(&prog_load_attr, &obj, &prog_fd);
  if (err) {
    fprintf(stderr, "ERR: loading bpf object file %s (%d): %s\n",
            filename, err, strerror(-err));
    return NULL;
  }

  ifindex = if_nametoindex(dev_name);
  if(!ifindex) {
    fprintf(stderr, "ERR: unable to lookup intreface index for dev %s\n", dev_name);
    return NULL;
  }

  err = bpf_set_link_xdp_fd(ifindex, prog_fd, xdp_flags);
  if (err < 0) {
    fprintf(stderr, "ERR: unable to attach to interface %s:%d: %s\n",
            dev_name, ifindex, strerror(errno));
  }
  printf("Successfully loaded PBX program %s to %s:%d\n", filename, dev_name, ifindex);
  return obj;
}

void unload_bpf_obj() {
  int ifindex;
  int err;

  ifindex = if_nametoindex(dev_name);

  if(!ifindex) {
    fprintf(stderr, "ERR: unable to lookup intreface index for dev %s\n", dev_name);
    return;
  }

  printf("freeing xdp program from %s:%d\n", dev_name, ifindex);
  err = bpf_set_link_xdp_fd(ifindex, -1, xdp_flags);
  if (err < 0) {
    fprintf(stderr, "ERR: unable to unload from interface %s:%d: %s\n",
            dev_name, ifindex, strerror(errno));
  }
}

int find_map_fd(struct bpf_object *bpf_obj, const char *mapname)
{
	struct bpf_map *map;
	int map_fd = -1;

	map = bpf_object__find_map_by_name(bpf_obj, mapname);
        if (!map) {
		fprintf(stderr, "ERR: cannot find map by name: %s\n", mapname);
		return map_fd;
	}

	map_fd = bpf_map__fd(map);
	return map_fd;
}

static int __check_map_fd_info(int map_fd, struct bpf_map_info *info,
			       struct bpf_map_info *exp)
{
  __u32 info_len = sizeof(*info);
  int err;

  if (map_fd < 0)
    return EXIT_FAILURE;

  /* BPF-info via bpf-syscall */
  err = bpf_obj_get_info_by_fd(map_fd, info, &info_len);
  if (err) {
    fprintf(stderr, "ERR: %s() can't get info - %s\n",
            __func__,  strerror(errno));
    return EXIT_FAILURE;
  }

  if (exp->key_size && exp->key_size != info->key_size) {
    fprintf(stderr, "ERR: %s() "
            "Map key size(%d) mismatch expected size(%d)\n",
            __func__, info->key_size, exp->key_size);
    return EXIT_FAILURE;
  }
  if (exp->value_size && exp->value_size != info->value_size) {
    fprintf(stderr, "ERR: %s() "
            "Map value size(%d) mismatch expected size(%d)\n",
            __func__, info->value_size, exp->value_size);
    return EXIT_FAILURE;
  }
  if (exp->max_entries && exp->max_entries != info->max_entries) {
    fprintf(stderr, "ERR: %s() "
            "Map max_entries(%d) mismatch expected size(%d)\n",
            __func__, info->max_entries, exp->max_entries);
    return EXIT_FAILURE;
  }
  if (exp->type && exp->type  != info->type) {
    fprintf(stderr, "ERR: %s() "
            "Map type(%d) mismatch expected type(%d)\n",
            __func__, info->type, exp->type);
    return EXIT_FAILURE;
  }

  return 0;
}

void intHandler(int s) {
  isRunning = 0;
}

int main (int argc, char **argv) {
  const char *stats_map = "xdp_stats_map";

  int err;

  int stats_map_fd;
  struct bpf_map_info map_expect = { 0 };
  struct bpf_map_info info = { 0 };

  struct bpf_object *bpf_obj = load_bpf_obj(xdp_filename);
  if (bpf_obj == NULL) {
    return EXIT_FAILURE;
  }

  atexit(unload_bpf_obj);

  stats_map_fd = find_map_fd(bpf_obj, stats_map);
  if (stats_map_fd < 0) {
    fprintf(stderr, "ERR: unable to lookup map %s\n", stats_map);
    exit(EXIT_FAILURE);
  }

  map_expect.key_size    = sizeof(__u32);
  map_expect.value_size  = sizeof(struct datarec);
  map_expect.max_entries = XDP_ACTION_MAX;
  err = __check_map_fd_info(stats_map_fd, &info, &map_expect);
  if (err) {
    fprintf(stderr, "ERR: map via FD not compatible\n");
    exit(err);
  }

  signal(SIGINT, intHandler);
  while (isRunning) {
    struct datarec value_drop;
    struct datarec value_pass;
    __u32 key;

    key = XDP_DROP;
    if (bpf_map_lookup_elem(stats_map_fd, &key, &value_drop) != 0) {
      fprintf(stderr, "unable too lookup DROP stats.\n");
      exit(EXIT_FAILURE);
    }

    key = XDP_PASS;
    if (bpf_map_lookup_elem(stats_map_fd, &key, &value_pass) != 0) {
      fprintf(stderr, "unable too lookup PASS stats.\n");
      exit(EXIT_FAILURE);
    }

    printf("DROP %llu,\tPASS %llu\n", value_drop.rx_packets, value_pass.rx_packets);
    sleep(1);
  }

  exit(EXIT_SUCCESS);
}
