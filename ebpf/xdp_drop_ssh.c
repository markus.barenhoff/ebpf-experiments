#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/bpf.h>

#include <bpf/bpf_helpers.h>

#ifndef __bpf_htons
# if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define __bpf_htons(x) __builtin_bswap16(x)
# else
#  define __bpf_htons(x) (x)
# endif
#endif

SEC("xdp_drop_ssh")
int xdp_drop_ssh_prog (struct xdp_md *ctx)
{
  void *data_end = (void *)(long)ctx->data_end;
  void *data = (void *)(long)ctx->data;
  void *cursor = data;


  // no ethernet frame ... drop it
  if (cursor + sizeof(struct ethhdr) > data_end)
    return XDP_DROP;

  struct ethhdr *eth = cursor;
  cursor += sizeof(struct ethhdr);

  // ipv4
  if (eth->h_proto == __bpf_htons(ETH_P_IP) && (cursor + sizeof(struct iphdr) <= data_end)) {
      struct iphdr *ipv4 = cursor;
      cursor += sizeof(struct iphdr);

      if (ipv4->protocol == IPPROTO_TCP && (cursor + sizeof(struct tcphdr) <= data_end)) {
        struct tcphdr *tcp = cursor;
        cursor += sizeof(struct tcphdr);

        if (tcp->dest == __bpf_htons(22))
          return XDP_DROP;
      }
    }

  return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
