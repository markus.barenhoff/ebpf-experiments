#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>

#include "xdp_drop_ssh_stats.h"

#ifndef __bpf_htons
# if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define __bpf_htons(x) __builtin_bswap16(x)
# else
#  define __bpf_htons(x) (x)
# endif
#endif

struct {
  __uint(type, BPF_MAP_TYPE_ARRAY);
  __type(key, __u32);
  __type(value, struct datarec);
  __uint(max_entries, XDP_ACTION_MAX);
} xdp_stats_map SEC(".maps");


/* LLVM maps __sync_fetch_and_add() as a built-in function to the BPF atomic add
 * instruction (that is BPF_STX | BPF_XADD | BPF_W for word sizes)
 */
#ifndef lock_xadd
#define lock_xadd(ptr, val)	((void) __sync_fetch_and_add(ptr, val))
#endif

static inline __attribute__((always_inline)) enum xdp_action log_xdp_action(enum xdp_action a) {
  struct datarec *rec;

  /* Lookup in kernel BPF-side return pointer to actual data record */
  rec = bpf_map_lookup_elem(&xdp_stats_map, &a);

  /* BPF kernel-side verifier will reject program if the NULL pointer
   * check isn't performed here. Even-though this is a static array where
   * we know key lookup XDP_PASS always will succeed.
   */
  if (!rec)
    return XDP_ABORTED;

  /* Multiple CPUs can access data record. Thus, the accounting needs to
   * use an atomic operation.
   */
  lock_xadd(&rec->rx_packets, 1);

  return a;
}

SEC("xdp_drop_ssh_stats")
int xdp_drop_ssh_stats_prog (struct xdp_md *ctx)
{
  void *data_end = (void *)(long)ctx->data_end;
  void *data = (void *)(long)ctx->data;
  void *cursor = data;

  // no ethernet frame ... drop it
  if (cursor + sizeof(struct ethhdr) > data_end)
    return log_xdp_action(XDP_DROP);

  struct ethhdr *eth = cursor;
  cursor += sizeof(struct ethhdr);

  // ipv4
  if (eth->h_proto == __bpf_htons(ETH_P_IP) && (cursor + sizeof(struct iphdr) <= data_end)) {
      struct iphdr *ipv4 = cursor;
      cursor += sizeof(struct iphdr);

      if (ipv4->protocol == IPPROTO_TCP && (cursor + sizeof(struct tcphdr) <= data_end)) {
        struct tcphdr *tcp = cursor;
        cursor += sizeof(struct tcphdr);

        if (tcp->dest == __bpf_htons(22))
          return log_xdp_action(XDP_DROP);
      }
    }

  return log_xdp_action(XDP_PASS);
}

char _license[] SEC("license") = "GPL";
