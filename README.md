# ebpf experiments


## Tooling

* Install BPF CompilerCollection (BCC)
** https://github.com/iovisor/bcc
* Install libbpf
* Install bpf tools
* Install xdp tools


# Tooling Cheat Sheet

* Compile: `clang -O2 -g -Wall -target bpf -c code.c -o code.o`
* Disassemble: `llvm-objdump -d (-S) code.o`
* Show Object Headers: `llvm-objdump -h code.o`
* Loading XDP program:
  * ip (does not support type maps): `sudo ip link set vtest1 xdpgeneric obj xdp_prog.o sec xdp_prog`
  * xdp-loader: `sudo xdp-loader load -m skb -s xdp_prog vtest1 xdp_prog.o`
* Show running BPF programs: `sudo bpftool prog show`
* Show running XDP programs: `sudo xdp-loader status`
* Unload XDP program:
  * ip: `sudo ip link set vtest1 xdpgeneric off`
  * xdp-loader: `sudo xdp-loader unload -a vtest1`
